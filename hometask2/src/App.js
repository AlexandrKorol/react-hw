import React from 'react';
import './App.css';
import LoaderImg from './components/task1/LoaderImg';
import ErrorCatch from './components/task1/ErrorCatch';
import Breadcrumbs from './components/task2/Breadcrumbs';
import Table from './components/task3/Table';
import Row from './components/task3/Row';
import Cell from './components/task3/Cell';
class App extends React.Component 
{
  state = {
    loaded: false,
  }

  render = () => {


    return (
      <div>
        <ErrorCatch>
          <LoaderImg
            src="https://media.springernature.com/lw630/nature-cms/uploads/cms/pages/2913/top_item_image/cuttlefish-e8a66fd9700cda20a859da17e7ec5748.png"
          />
        </ErrorCatch>
        <Breadcrumbs
          items={[
            {
              path: '/#/',
              name: 'Main'
            },
            {
              path: '/#/category',
              name: 'All categories'
            },
            {
              path: '/#/category/1',
              name: 'Cats and dogs accessories'
            }
          ]}
          />
          <Table>
    <Row head="true">
      <Cell type="" background="red">#</Cell>
      <Cell type="date">2</Cell>
      <Cell type="number">3</Cell>
      <Cell type="money" currency="$">4</Cell>
    </Row>
    <Row>
      <Cell type="" background="red">1</Cell>
      <Cell type="date">2</Cell>
      <Cell type="number">3</Cell>
      <Cell type="money" currency="$">4</Cell>
    </Row>
  </Table>


    </div>
    )}
  }  
export default App;