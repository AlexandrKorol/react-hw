import React from 'react';

class ErrorCatch extends React.Component{

    state={
        error: false
    }
    componentDidCatch( error, info ){
        console.log(error, info );
        this.setState({
            error: true
        })

    }
    
    render = () => {
        const {error} = this.state;
        if(error){
            return(
                <div> Something went wrong :( </div>
            )
        } 
        return this.props.children;
    }


}

export default ErrorCatch;