import React from 'react';
import gear from './gear.png';

class Loader extends React.Component{
    state={
        loaded:true
    }
    componentDidMount(){
        setTimeout(()=>{
            this.setState({
                loaded: false
            })
            
        },3000)
        
    }
    render =()=>{
        const {loaded}= this.state;
        if(loaded){
            return <div className="loader-container"><div className="loader"><div className="text">Loading...</div>
            <img className="gear gear1" src={gear} alt="Gear img"></img>
            <img className="gear gear2" src={gear} alt="Gear img"></img>
            <img className="gear gear3" src={gear} alt="Gear img"></img></div></div>
        }
        return(
            <div className="loader-container"><div className="loader"><img src ={this.props.src} alt ="Img for hometask"></img></div></div>
        )
    }
}
export default Loader;