import React from 'react';

const DateCell = (props) => {
    return <div className="date cell"style={props.style}>
    {props.children}
</div>
}


const MoneyCell = (props) =>{
    if(props.currency === undefined)
    {
        console.log("Warning. You haven't chosen any currency");
    }
    return <div className="money cell"style={props.style}>
    {props.children}
    {props.currency}
</div>
}


const NumberCell = (props) =>{
    return <div className="number cell" style={props.style}>
    {props.children}
</div>
}


const TextCell = (props) =>{
    return <div className="cell" style={props.style}>
    {props.children}
</div>
}


const Cell = (props) =>{
    console.log(props.cells)
    const style = {
        width: 50 * props.cells + 'px',
        background : props.background,
        color: props.color
    } 

    if( props.type === 'date'){
        return <DateCell style={style} {...props} />
    }
    if( props.type === 'number'){
        return <NumberCell {...props} style={style}/>
    }
    if(props.type === 'money')
    {
       return <MoneyCell {...props} style={style}/>
    }
    if(props.type === 'text' || props.type === ''){
        return <TextCell {...props} style={style}/>
    }

    return null;
}
Cell.defaultProps = {
    type: 'text',
    cells: 1,
    background: 'transparent',
    color: 'black'
  }
export default Cell;