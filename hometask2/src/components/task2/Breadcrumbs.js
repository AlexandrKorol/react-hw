import React from 'react';

const Breadcrumbs = ({items}) =>{
    return(<div>
        <div></div>
       <ul className="breadcrumbs">{
           items.map((obj,key)=>{
               return <li key={key} className="breadcrumbs-item"><a href={obj.path}>{obj.name}</a> / </li>
           })
       }</ul>
       </div>
    )
}
export default Breadcrumbs;