import React from 'react';

function GuestItem(props){
    
    return <li className="d-flex mx-auto justify-content-between my-3 ">
    <div>
    Гость <strong>{props.guest.name}</strong> работает в компании <strong>"{props.guest.company}"</strong>.
    <div>
        Его контакты
    </div>
    <div><strong>{props.guest.phone}</strong></div>
    <div><strong>{props.guest.address}</strong></div>
    
    </div>
    <button className="btn btn-success h-50" onClick = {props.arrived} data-index={props.guest.index}>Прибыл</button>
    
</li>
}
export default GuestItem;