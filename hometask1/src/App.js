import React, { Component } from 'react';
import guestList from './classWork_guests.json';
import GuestItem from './GuestItem';
import './App.css';




const guestData = guestList.map(guest => {
  guest.arrived = false;
  return guest;
})

class App extends Component {


  state = {
    guests: guestData
  }

  arrived = (e) => {
    const index = e.target.dataset.index;
    
    let newGuests = this.state.guests.map(item => {
      if(parseInt(index) === parseInt(item.index)){
        item.arrived = !item.arrived;
      }
      return item;
    });
   
    this.setState({
      guests: newGuests
  });
    e.target.parentNode.classList.toggle('arrived');

  }

  searchUser = (e) => {
    let query = e.target.value.toLowerCase();
    let search = guestList.filter((item) => {
        let keys = Object.keys(item);
        let reuslt = keys.some( key => {
          return String(item[key]).toLowerCase().indexOf(query) !== -1;
        });

        return reuslt;
    }); 
    this.setState({
      guests: search
    })
  }

  render = () => {
    const { guests } = this.state;
    console.log(guestList)
    return (
      <div className="col-4 mx-auto">
          <div className="d-flex">
            <h1 className="mr-auto">Список гостей</h1>
            <h3 className="victim"><s>Список жертв</s></h3>
          </div>
        <div className="d-flex">
          <input onInput={this.searchUser} className="mx-auto pl-4" placeholder="Введите имя для поиска"/>
        </div>
        
        <ul className="ml-0 pl-0">
        {
          guests.length === 0 ? <p className="text-center">Nothing found :*(</p> : ''
        }
          {
            guests.map((guest, key) => (
              <GuestItem
                key={key}
                guest={guest}
                arrived={this.arrived}

              />

            ))
          }
        </ul>
      </div>
    );
  }
}

export default App;
