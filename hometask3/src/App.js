import React from 'react';
import MyChart from './components/task1/MyChart';
import Task2 from './components/task2/Uploader';
import Input from './components/task2/components/Input';
import './App.css';

class App extends React.Component {
  state = {
    custominput:
    {
      task2Input:
      {
        name: 'task2Input',
        placeholder: 'Task2Input',
        title: 'Task2Input title',
        contentMaxLength: 4,
        value: ''
      }
    }
  }
  onChangeHandler = (key) => (e) => {
    this.setState({
      custominput: {
        ...this.state.custominput,
        [key]: {
          ...this.state.custominput[key],
          value: e.target.value,
          contentLength: e.target.value.length
        }
      }

    })
  }
  render() {
    return (
      <div className="App">
        <MyChart />
        <p>Custom input</p>
        <Input {...this.state.custominput.task2Input} onChangeHandler={this.onChangeHandler('task2Input')} />
        <p>My custom input redactor</p>
        <Task2 />
      </div>
    )
  }
}

export default App;
