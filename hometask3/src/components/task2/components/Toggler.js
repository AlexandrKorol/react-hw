import React from 'react';

export class Toggler extends React.Component{
    render(){
        let {children, activeToggler, changeStatus, customkey, key2 } = this.props;
        return(
          <div>
            <div className="togglerContainer">
              {
                React.Children.map(
                  children, 
                  (childrenItem) => { 

                    
                    return React.cloneElement(childrenItem, {
                      ...childrenItem.props,
                      active: childrenItem.props.value === activeToggler ? true : false,
                      changeStatus: changeStatus,
                      customkey: customkey,
                      key2: key2
                    })
                  }
                )
                }
            </div>
          </div>
        );
    }
}


export const TogglerItem =({name,value,active, changeStatus,customkey, key2})=>{
    return(
        <div className ={
            active === true ?
            'togglerItem active' :
            'togglerItem'

        }
        onClick = {
            changeStatus !== undefined ?
            changeStatus(customkey,value, key2, name):
            null
        }
        >
            {name}
        </div>
    )
}

