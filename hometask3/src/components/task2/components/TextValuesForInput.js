import React from 'react';

const TextValuesForInput = (props) =>{

    return(
        <div className="inputFields">
            <input onChange={props.inputRedactor} name="placeholder"placeholder="Write here a placeholder you want to see"></input>
            <input onChange={props.inputRedactor} name="value"placeholder="Write here the default value you want to see" maxLength={props.contentMaxLength}></input>
            <input onChange={props.inputRedactor} name="contentMaxLength"placeholder="Write here max length of symbols allowed you want to see"></input>
        </div>
    )
}
export default TextValuesForInput;