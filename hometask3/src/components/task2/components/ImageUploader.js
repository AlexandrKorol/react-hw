import React from 'react';
import PropTypes from 'prop-types';

const Input = ({uploadMe, url}) =>{

   

    return(
        <div className="uploader">
                
        <label htmlFor="picture"><img id="target" src={url} alt="wallpapers"/></label>
        <input type="file" id="picture" name="picture" accept=".png, .jpg, jpeg" onChange={uploadMe}>
        </input>
        
    </div>
    )
}
Input.propTypes = {
    uploadMe: PropTypes.func.isRequired,
    url: PropTypes.string.isRequired
}
export default Input;