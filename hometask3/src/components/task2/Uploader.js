import React from 'react';
import img from './placeimg_400_400_any.jpg';
import ImageUploader from './components/ImageUploader';
import { Toggler, TogglerItem } from './components/Toggler';
import TextValuesForInput from './components/TextValuesForInput';
class Uploader extends React.Component {
    state = {
        url: img,
        // Для налаштувань інпута
        type: "text",
        name: "Alexandr",
        placeholder: "Placeholder for editing",
        value: '',
        contentLength: null,
        contentMaxLength: null,
        firstTogglerStatus: 'text',
    }
    // Для завантаження картинки
    uploadMe = (e) => {
        if (e.target.files && e.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                this.setState({ url: reader.result });
            };
            reader.readAsDataURL(e.target.files[0]);
            console.log(reader.result);

        };



    }
    inputRedactor = (e) => {
        const stateName = e.target.name;
        const stateValue = e.target.value;
        if (stateName === 'value') {
            this.setState({
                contentLength: stateValue.length
            })
        }

        this.setState({
            [stateName]: stateValue
        });
    };
    changeStatus = (customkey, value, StateName, StateValue) => (e) => {
        this.setState({
            [customkey]: value,
            [StateName]: StateValue
        })
    }
    render() {
        const { url, firstTogglerStatus, type, placeholder, value, contentLength, contentMaxLength } = this.state;
        const { uploadMe, changeStatus, inputRedactor } = this;
        return (
            <div>

                <form>
                    <Toggler
                        activeToggler={firstTogglerStatus}
                        changeStatus={changeStatus}
                        customkey='firstTogglerStatus'
                        key2="type"
                    >
                        <TogglerItem name="text" value="text" />
                        <TogglerItem name="file" value="file" />
                        <TogglerItem name="data" value="data" />
                        <TogglerItem name="password" value="password" />
                    </Toggler>

                    <TextValuesForInput
                        inputRedactor={inputRedactor} contentMaxLength={contentMaxLength}
                    />

                    <div className="resultInput">
                        <p>Here is your result input:</p>
                        <input type={type} value={value} placeholder={placeholder} maxLength={contentMaxLength}
                        onChange={inputRedactor} name="value"></input>
                        <p>The count of symbols used: {contentLength}</p>
                        <button onClick={(e)=>{e.preventDefault();
                            console.log(this.state)}} className="checker">Show me the state</button>
                    </div>

                    <ImageUploader url={url} uploadMe={uploadMe} />

                </form>
            </div>
        )
    }
}

export default Uploader;