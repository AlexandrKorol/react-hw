import React from 'react';
import Chart from 'chart.js';

class MyChart extends React.Component{
    state={
        values: [0, 10, 5, 2, 20, 30, 45]
      }
      chart = null;
      chartRef = React.createRef();
      componentDidMount(){
        const myChartRef = this.chartRef.current.getContext("2d");
        this.chart = new Chart(myChartRef,{
          type: 'line',
          data: {
              labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
              datasets: [{
                  label: 'Hometask3 graph',
                  backgroundColor: 'rgb(255, 99, 132)',
                  borderColor: 'rgb(255, 99, 132)',
                  data: this.state.values
              }]
          },
          options: {}
        }
    )
      }
    
      changeData = () =>{
        const min = 1;
        const max = 100;
        let result =[];
        for(var i =0 ;i<7;i++){
          result[i]=Math.floor(Math.random()*(max-min+1)+min)
        }
        this.setState({
          values:result
        });
        
      }
    
      componentDidUpdate(){
        this.chart.config.data.datasets[0].data = this.state.values;
        this.chart.update();
      }
    
    render(){
        return(
            <div>
                <canvas ref={this.chartRef}>
  
  </canvas>
  <button onClick={this.changeData}>
    Change Data
  </button>
            </div>
        )
    }
}
export default MyChart;